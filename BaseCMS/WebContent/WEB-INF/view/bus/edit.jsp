<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}
</style>
</head>
<body>
	<%@ include file="/WEB-INF/view/common.jsp"%>
	<div class="form-container">
		<div>
			<span class="title">包车管理</span> <img
				src="<c:url value="/images/jiantou.png" />" /> <span
				class="sec-title">包车管理</span> <img
				src="<c:url value="/images/jiantou.png" />" /> <span
				class="sec-title">修改</span> <span style="float: right;"><input
				type="button" onclick="history.go(-1)" value="返回"
				class="btn btn-default"></span>
		</div>
		<hr />
	</div>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form action="notice/add" name="addNoticeForm" id="addNoticeForm"
				method="post" enctype="multipart/form-data"
				class="form-inline form-index">

				<div style="margin: 15px;">
					<input type="hidden" name="id" id="id" value="${item.id }" /> <font
						color="red">*</font><span class="preTitle">出发地:</span> <input
						name=fromAddr id="fromAddr" style="width: 300px"
						value="${item.fromAddr}" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">目的地:</span> <input
						name="toAddr" id="toAddr" style="width: 300px"
						value="${item.toAddr}" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">出发时间:</span><input
						name="goDate" id="goDate" type="text" class="form-control"
						value="${item.goDate}" style="width: 170px;" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">客单价:</span><input
						name="price" id="price" style="width: 300px" class="form-control"
						value="${item.price}" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">座位数:</span><input
						name="seats" id="seats" style="width: 300px" class="form-control"
						value="${item.seats}" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">状态:</span> <select
						name=status id="status" class="form-control">
						<option value="0"
							<c:if test="${item.status=='0' }">selected="selected"</c:if>>正常</option>
						<option value="1"
							<c:if test="${item.status=='1' }">selected="selected"</c:if>>关闭</option>
					</select>
				</div>
				<div style="margin: 15px;">
					<span class="preTitle">备注:</span>
					<textarea rows="5" cols="60" id="remark" name="remark"> ${item.remark}</textarea>
				</div>
				<div style="margin: 15px;">
					<input type="button" onclick="modify();" value="保存"
						class="btn btn-default"> <input type="button" value="取消"
						onclick="history.go(-1)" class="btn btn-default">
				</div>
			</form>
		</div>
	</div>
	<script>
		function modify() {
			if ($('#fromAddr').val() == "") {
				$("#alert-content").html("请输入出发地点！");
				$("#alert-modal").modal("show");
				$('#fromAddr').focus();
				return false;
			}
			if ($('#toAddr').val() == "") {
				$("#alert-content").html("请输入目的地点！");
				$("#alert-modal").modal("show");
				$('#toAddr').focus();
				return false;
			}
			if ($('#goDate').val() == "") {
				$("#alert-content").html("请选择出发时间！");
				$("#alert-modal").modal("show");
				$('#goDate').focus();
				return false;
			}
			if ($('#price').val() == "") {
				$("#alert-content").html("请输入客单价！");
				$("#alert-modal").modal("show");
				$('#price').focus();
				return false;
			}
			if ($('#seats').val() == "") {
				$("#alert-content").html("请输入座位数！");
				$("#alert-modal").modal("show");
				$('#seats').focus();
				return false;
			}
			$.ajax({
				url : "edit",
				data : {
					id : $("#id").val(),
					fromAddr : $("#fromAddr").val(),
					toAddr : $("#toAddr").val(),
					goDate : $("#goDate").val(),
					price : $("#price").val(),
					seats : $("#seats").val(),
					remark : $("#remark").val(),
					status : $("#status").val()
				},
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.flag) {
						$("#alert-content").html("操作成功");
						$("#alert-ok").bind("click", function() {
							window.location.href = "../bus";
						});
						$("#alert-modal").modal("show");
					} else {
						$("#alert-content").html("操作失败");
						$("#alert-modal").modal("show");
					}
				},
				/* error : function(result) { $.messager.alert("提示", "操作失败：" +
				result.message); } */error : function() {
					$("#alert-content").html("系统异常,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		}
	</script>
</body>
</html>