<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index" id="form">
				<div id="uploadImg" class="item">
					<font color="red"></font><span class="preTitle">栏目:</span> <select
						name="catgoryId" class="form-control">
						<c:forEach items="${catgory }" var="item">
							<option value="${item.id }">${item.name }</option>
						</c:forEach>
					</select>
				</div>
				<div id="uploadImg" class="item">
					<font color="red"></font><span class="preTitle">标题:</span> <input
						name="title" id="title" type="text" class="form-control" />
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">公众号:</span> <input
						name="wxAccount" id="wxAccount" type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="white"></font> <span class="preTitle">摘要:</span> <input
						name="remark" id="remark" type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="red"></font> <span class="preTitle">状态:</span>
					<html:select cssClass="form-control" collection="articleStatus"
						selectValue="${article.status }" name="status" id="status">
					</html:select>
				</div>
				<div class="item">
					<font color="white"></font> <span class="preTitle">正文:</span>
					<div class="upImg">
						<script type="text/plain" id="container" name="content" disabled
							style="width:1000px;height:240px;"></script>
					</div>
				</div>
				<input type="button" value="保存" class="btn btn-default"
					onclick="doSubmit();">
			</form>
		</div>
	</div>
	<script type="text/javascript">
		//实例化编辑器
		var ue = UE.getEditor('container');
		function doSubmit() {
			if ($("#title").val() == "") {
				layer.alert("标题不能为空");
				return;
			}
			if ($("#wxAccount").val() == "") {
				layer.alert("公众号不能为空");
				return;
			}
			if (ue.getContent() == "") {
				layer.alert("正文不能为空");
				return;
			}
			var data = $("#form").serialize();
			$.ajax({
				url : "add",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						}, closeWindow());
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('操作失败:系统异常');
				}
			});
		}
		function closeWindow() {
			parent.reload();
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.layer.close(index);
		}
		function ajaxFileUpload(id) {
			//执行上传文件操作的函数
			$
					.ajaxFileUpload({
						//处理文件上传操作的服务器端地址(可以传参数,已亲测可用)
						url : '../common/fileUpload',
						secureuri : false, //是否启用安全提交,默认为false 
						fileElementId : id, //文件选择框的id属性
						dataType : 'json', //服务器返回的格式,可以是json或xml等
						success : function(result, status) { //服务器响应成功时的处理函数
							if (result.success) {
								$(":input[name=" + id + "]").val(result.obj);
								$(":input[name=" + id + "]")
										.prev()
										.html(
												"<img  style='vertical-align:middle;' width='200' height='120' src='"
														+ result.obj
														+ "'/> <input type='button' class='btn btn-default' onclick='ajaxFileDelete(this)' value='删除图片'/> <input type='button' class='btn btn-default' onclick='openImg(this)' value='查看原图'/>");
								$("div[id='" + id + "_div']").css("margin-top",
										"78px");
							} else {
								$("#alert-content").html(
										"操作失败：" + result.message);
								$("#alert-modal").modal("show");
							}
						},
						error : function(data, status, e) { //服务器响应失败时的处理函数
							$("#alert-content").html("文件服务器异常,请联系管理员！");
							$("#alert-modal").modal("show");
						}
					});
		}
		function ajaxFileDelete(node) {
			var imgNode = $(node).prev();
			var nextNode = $(node).next();
			$(node).remove();
			$(imgNode).remove();
			$(nextNode).remove();
		}
		function openImg(that) {
			var imgNode = $(that).prev().prev();
			window.open($(imgNode).attr("src"));
		}
	</script>
</body>
</html>