<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>爱雅安</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
</head>
<body>
	<div class="outDiv">
		<div class="container con-container">	
			<img src="<c:url value="/images/portal/proCase/aiyaan.png"/>" class="img"/>
			<div class="content-one"><span style="color:#000;"><b>中国农业银行</b></span>携手卡卡付进行深度行业定制，结合卡卡付协议支付产品，实现企业客户跨行资金归集，满足大客户跨行代收需求。通过“网页端+手机客户端”模式，实现代收银行卡管理、银行卡绑定、资金代收等功能。</div>
			<div class="content-one">卡卡付协议支付产品，通过协议完成资金流的转换，将代收银行卡的资金，归集到企业用户指定的银行账户下，为企业提供方便、快捷的收款服务。</div>
		</div>
	</div>
</body>
</html>