<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<title>常见问题</title>
<style type="text/css">
body {
	font-size:18px;
	background:#F5F5F5;
	padding:10px 0;
}
.content {
	border-radius: 2px;
	overflow: auto;
	margin-bottom:10px;
	word-break: break-all;
}
.content .con {
	color:#7D7D7D;
}
</style>
</head>
<body>
	<div class="content">
		<div class="title">Q：商户如何入网？</div>
		<div class="con">A：商户可以免费下载和安装客户端，注册前需联系代理商获取邀请码，注册成功，即可开通相关交易。</div>
	</div>
	<div class="content">
		<div class="title">Q：注册成功后信息能否修改？</div>
		<div class="con">A：商户注册成功后，所有信息都不能通过客户端修改，如需修改，请联系代理商或客服申请修改。</div>
	</div>
	<div class="content">
		<div class="title">Q：收款资金如何结算？</div>
		<div class="con">A：商户注册成功即可使用收款等功能，开通结算则需要上传资料后申请开通，我们将在2~3个工作日内完成资料审核，审核成功后将在下个工作日开始结算；结算最小起结金额为100.00元。</div>
	</div>
	<div class="content">
		<div class="title">Q：是否所有的交易都能够撤销？</div>
		<div class="con">A：不是，只有当日成功的交易才能撤销；同时，开通了即时提款业务且已经完成了至少一笔提款交易后，交易撤销功能将关闭；在本次收款结算完成后，交易撤销将会重新开启。</div>
	</div>
	<div class="content">
		<div class="title">Q：如何绑定设备？</div>
		<div class="con">A：交易时，刷卡设备将与当前账户绑定；设备首次使用并未绑定时，客户端会提示设备与当前登录账户绑定，此时请仔细核对并确认是否需要绑定设备；设备一旦绑定成功，将不能再与其他账户绑定。</div>
	</div>
	<div class="content">
		<div class="title">Q：刷卡器与账号绑定后，如何解绑？</div>
		<div class="con">A：设备绑定后，如需解绑，请联系代理商或拨打客服。</div>
	</div>
	<div class="content">
		<div class="title">Q：解绑后，刷卡设备为什么不能再次使用？</div>
		<div class="con">A：解绑后，刷卡设备将被冻结，在确认设备正常后，如需再次使用，请联系代理商或客服。</div>
	</div>
	<div class="content">
		<div class="title">Q：有芯片的银行卡，是否可以通过刷卡交易呢？</div>
		<div class="con">A：当银行卡带有芯片时，必须使用芯片进行交易，否则交易可能被拒绝。</div>
	</div>
	<div class="content">
		<div class="title">Q：信用卡无密码，是否可以支付？</div>
		<div class="con">A：信用卡无密码时，建议随机输入密码尝试支付。</div>
	</div>
	<div class="content">
		<div class="title">Q：为什么刷卡设备总是读卡失败？</div>
		<div class="con">A：读卡时请注意以下事项：<br>
			<span>1)请保持刷卡器设备与手机通讯正常；</span><br>
			<span>2)检测银行卡磁条，磁性过低或有划痕时容易刷卡失败；</span><br>
			<span>3)刷卡的速度过快、过慢或停顿均会导致刷卡失败，请调整刷卡速度，平稳刷卡；</span><br>
			<span>4)磁头出现故障，无法正常刷卡；</span><br>
			<span>5)银行卡有芯片时，必须使用芯片进行交易，插入卡槽读卡时，请保持卡片与设备接触良好。</span>
		</div>
	</div>
	<div class="content">
		<div class="title">Q：为什么我的设备之前能正常使用，突然就不能使用了？</div>
		<div class="con">A：采用蓝牙通讯的设备，可能存在以下几个原因：<br>
			<span>1)设备与手机的配对出现异常，请重新检查配对；</span><br>
			<span>2)请检查设备电量是否充足；</span><br>
			<span>3)刷卡器故障，请联系代理商维修；</span><br>
			采用NFC方式刷卡，可能存在以下几个原因：
			<span>1)手机系统NFC软件模块异常，请重启手机尝试；</span><br>
			<span>2)由于部分手机的NFC芯片不兼容，导致无法读取部分银行的IC卡芯片，请更换银行卡或手机尝试。</span>
		</div>
	</div>
	<div class="content">
		<div class="title">Q：如果刷卡设备遗失了，该如何办理？</div>
		<div class="con">A：如果刷卡设备遗失，请联系客服中心挂失。</div>
	</div>
	<div class="content">
		<div class="title">Q：换手机终端了还能继续使用吗？</div>
		<div class="con">A：可以的，重新安装客户端版本即可。</div>
	</div>
</body>
</html>