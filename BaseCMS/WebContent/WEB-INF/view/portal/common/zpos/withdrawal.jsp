<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<title>即时提款业务说明</title>
<style type="text/css">
body {
	font-size:18px;
	background:#F5F5F5;
	padding:10px 0;
}
.title {
	text-align:center;
	font-weight:bold;
	margin-bottom:20px;
}
.content {
	border-radius: 2px;
	overflow: auto;
	margin-bottom:10px;
	word-break: break-all;
}

</style>
</head>
<body>
	<div class="title">“即时提款”业务规则说明</div>
	<div class="content">1. “即时提款”为您提供当日提款服务，是将当日收款款项，即待结算资金中部分或全部提前提取，当日到账。</div>
	<div class="content">2. 该服务未开通时，请联系您的代理商申请开通。</div>
	<div class="content">3. 基本申请资格： <br>
		<span>1）已成功注册并上传资料的商户；</span><br>
		<span>2）商户审核通过，已开通结算；</span><br>
		<span>3）商户结算账户为对私账户；</span><br>
		<span>4）商户结算账户支持即时提款。</span>
	</div>
	<div class="content">4. 使用此服务时，即表示您认可该业务模式及相关的收费标准。</div>
	<div class="content">5. 即时提款单笔和单日交易限额，以服务规则定义为准。</div>
	<div class="content">6. 使用此服务时，可用提款金额为待结算金额减去已提款金额。</div>
	<div class="content">7. 使用此服务时，请确认您的提款金额、手续费用、结算账户等信息，以免有误。</div>
	<div class="content">8. 手续费将从可提款金额中自动扣除；提款时，请确认可提款金额足够扣除手续费和提款金额。</div>
	<div class="content">9. 在成功完成一笔即时提款后，当天的撤销交易功能将被关闭，结算完成后撤销交易功能将重新开启。</div>
	<div class="content">10. 您申请提取的金额预计将在2小时内到达您的结算账户，具体到账时间以银行处理结果为准。</div>
</body>
</html>