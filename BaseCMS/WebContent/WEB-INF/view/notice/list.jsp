<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<div class="table-container" id="content">
	<table>
		<thead>
			<tr>
				<th align="center" width="200px">创建时间</th>
				<th align="center" width="200px">修改时间</th>
				<th align="center" width="100px">创建人</th>
				<th align="center" width="120px">公告类型</th>
				<th align="center">标题</th>
				<th align="center">内容</th>
				<th align="center" width="80px">状态</th>
				<th align="center" width="140px">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listDto.notices}" var="notice">
				<tr>
					<td align="center"><fmt:formatDate
							value="${notice.createDate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
					</td>
					<td align="center"><fmt:formatDate
							value="${notice.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
					</td>
					<td align="center">${notice.createUserName}</td>
					<td align="center"><html:span id="type" name="type"
							collection="noticeType"
							selectValue="${notice.type}">
						</html:span>
						</td>
					<td align="center">${notice.title}</td>
					<td align="center">${notice.contents}</td>
					<td align="center">${STATEMAP[notice.status]}</td>
					<td align="center"><c:if test="${notice.status=='00'}">
							<html:auth res="/notice/modify">
								<a target="_blank" href="notice/modify?id=${notice.id}">修改
								</a>&nbsp;
						</html:auth>
							<html:auth res="/notice/cancle">
								<a target="rightFrame" onclick="isDel('${notice.id}');"
									href="javascript:void(0);">作废 </a>&nbsp;
						</html:auth>
							<html:auth res="/notice/view">
								<a target="_blank" href="notice/view?id=${notice.id}">详情
								</a>
							</html:auth>
						</c:if> <c:if test="${notice.status!='00'}">
							<html:auth res="/notice/view">
								<a target="_blank" href="notice/view?id=${notice.id}">详情</a>
							</html:auth>
						</c:if></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#page').pagination({
		total : '${listDto.count}',
		pageSize : '${listDto.pageSize}',
		onSelectPage : function(pageNumber, pageSize) {
			queryPage(pageNumber, pageSize);
		}
	});
</script>
</html>