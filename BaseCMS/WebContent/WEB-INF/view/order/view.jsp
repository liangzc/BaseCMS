<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}
</style>
</head>
<body>
	<%@ include file="/WEB-INF/view/common.jsp"%>
	<div class="form-container">
		<div>
			<span class="title">包车管理</span> <img
				src="<c:url value="/images/jiantou.png" />" /> <span
				class="sec-title">订单管理</span> <img
				src="<c:url value="/images/jiantou.png" />" /> <span
				class="sec-title">详情</span> <span style="float: right;"><input
				type="button" onclick="history.go(-1)" value="返回"
				class="btn btn-default"></span>
		</div>
		<hr />
	</div>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form action="notice/add" name="addNoticeForm" id="addNoticeForm"
				method="post" enctype="multipart/form-data"
				class="form-inline form-index">

				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">用户ID:</span> <input
						name=fromAddr id="fromAddr" style="width: 300px"
						value="${item.userId}" disabled="disabled" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">用车Id:</span> <input
						name=fromAddr id="fromAddr" style="width: 300px"
						value="${item.busId}" disabled="disabled" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">出发地:</span> <input
						name=fromAddr id="fromAddr" style="width: 300px"
						value="${item.fromAddr}" disabled="disabled" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">目的地:</span> <input
						name="toAddr" id="toAddr" style="width: 300px"
						value="${item.toAddr}" disabled="disabled" class="form-control" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">出发时间:</span><input
						name="goDate" id="goDate" type="text" class="form-control"
						value="${item.goDate}" disabled="disabled" style="width: 170px;" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">客单价:</span><input
						name="price" id="price" style="width: 300px" class="form-control"
						value="${item.price}" disabled="disabled" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">座位数:</span><input
						name="seats" id="seats" style="width: 300px" class="form-control"
						value="${item.seats}" disabled="disabled" />
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> <span class="preTitle">状态:</span>
					${item.statusName}
				</div>
				<div style="margin: 15px;">
					<input type="button" onclick="history.go(-1)" value="返回"
						class="btn btn-default">
				</div>
			</form>
		</div>
	</div>
</body>
</html>