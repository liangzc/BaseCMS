<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}

.upImg {
	margin-left: 95px;
	margin-top: 10px;
}

.item {
	margin: 15px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index" id="form">
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">栏目编号:</span> <input
						name="name" id="name" type="text" value="${entity.id }" disabled
						class="form-control" />
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">栏目名称:</span> <input
						name="name" id="name" type="text" value="${entity.name }" disabled
						class="form-control" />
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">公众号:</span> <input
						name="wxAccount" id="wxAccount" type="text" value="${entity.wxAccount }" disabled
						class="form-control" />
				</div>
				<div class="item">
					<font color="red">*</font><span class="preTitle">栏目状态:</span>
					<%-- <html:select cssClass="form-control" collection="catgoryStatus" disabled
						selectValue="${entity.status }" name="status" id="status">
					</html:select> --%>
				</div>

				<div class="item">
					<font color="white">*</font> <span class="preTitle">显示顺序:</span> <input
						name="showOrder" id="showOrder" value="${entity.showOrder }" disabled
						type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="white">*</font> <span class="preTitle">栏目描述:</span> <input
						name="description" id="description" type="text" disabled
						value="${entity.description }" class="form-control" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>