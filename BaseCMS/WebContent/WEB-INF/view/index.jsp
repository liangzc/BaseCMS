<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CMS</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/themes/default/easyui.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/themes/icon.css"/>'>
	<style type="text/css">
		.menuitem {
			cursor: hand;
		}
	</style>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/themes/default/easyui.css"/>'>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/themes/icon.css"/>'>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/common.css"/>'>
<link rel="stylesheet" type="text/css" href='<c:url value="/css/index_page.css"/>'>
<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery.easyui.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/easyui-lang-zh_CN.js"/>"></script>
</head>
<body style="height: 100%; height: 100%">
	<div class="easyui-layout" style="width: 100%; height: 100%">
		<div data-options="region:'north'" style="height: 100px">
			<div style="float: right">
				<a href='<c:url value="/logout" />'>退出登录</a>
			</div>
		</div>
		<div data-options="region:'south',split:true"
			style="height: 26px; text-align: right;">
			<span>当前用户: <sec:authentication property="name" /></span>
		</div>
		<div data-options="region:'west',split:true,title:'功能'"
			style="width: 150px;">
			<div class="easyui-accordion">
				<c:forEach items="${menus}" var="menu">
					<div title="${menu.title}">
						<c:forEach items="${menu.children}" var="child">
							<div class="menuitem" title="${child.title}" url="${child.url}">${child.title}</div>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
		</div>
		<div data-options="region:'center'" id="maintable">
			
		</div>
	</div>
	<script type="text/javascript">	
		$(function() {
			document.title='CMS后台管理系统';
			var maintable = $("#maintable").tabs();
			$('.menuitem').click(function() {
				if (maintable.tabs('exists', this.title)) {
					maintable.tabs('select', this.title);
				} else {
					maintable.tabs('add', {
						title: this.title,
						href: $(this).attr('url'),
						closable: true,
						selected: true,
					});
				}
			});
		});		
		function _alert(){
			
		}
	</script>
</body>
</html>