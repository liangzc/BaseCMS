package com.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_cms_operatelog")
public class OperateLog implements Serializable {
	private static final long serialVersionUID = 1L;
    
	public static String OPSTATE_SUCCESS="00";
	public static String OPSTATE_ERROR="01";
	
	private Integer id;
	private Date opDate;
	private String opPerson;
	private String opState;
	private String opModule;
	private String opMethodName;
	private String opCnMethodName;
	private String opRemark;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=32 )
	public String getOpPerson() {
		return opPerson;
	}

	public void setOpPerson(String opPerson) {
		this.opPerson = opPerson;
	}

	@Column
	public String getOpMethodName() {
		return opMethodName;
	}

	public void setOpMethodName(String opMethodName) {
		this.opMethodName = opMethodName;
	}

	@Column
	public String getOpCnMethodName() {
		return opCnMethodName;
	}

	public void setOpCnMethodName(String opCnMethodName) {
		this.opCnMethodName = opCnMethodName;
	}

	@Column
	public Date getOpDate() {
		return opDate;
	}

	public void setOpDate(Date opDate) {
		this.opDate = opDate;
	}

	@Column
	public String getOpRemark() {
		return opRemark;
	}

	public void setOpRemark(String opRemark) {
		this.opRemark = opRemark;
	}

	@Column
	public String getOpState() {
		return opState;
	}

	public void setOpState(String opState) {
		this.opState = opState;
	}
	@Column
	public String getOpModule() {
		return opModule;
	}

	public void setOpModule(String opModule) {
		this.opModule = opModule;
	}

	@Override
	public String toString() {
		return "OperateLog [id=" + id + ", opDate=" + opDate + ", opPerson="
				+ opPerson + ", opState=" + opState + ", opModule=" + opModule
				+ ", opMethodName=" + opMethodName + ", opCnMethodName="
				+ opCnMethodName + ", opRemark=" + opRemark + "]";
	}

}
