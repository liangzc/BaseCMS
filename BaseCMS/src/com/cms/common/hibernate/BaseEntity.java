package com.cms.common.hibernate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/** 实体类基类. */
@SuppressWarnings("serial")
@MappedSuperclass
public class BaseEntity implements Serializable {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	/** 创建日期 */
	@Column(updatable = false)
	protected Date createDate;
	@Transient
	protected Date createDate_start;// 仅供查询使用
	@Transient
	protected Date createDate_end;// 仅供查询使用
	/** 修改日期 */
	protected Date modifyDate;
	@Transient
	protected Date modifyDate_start;// 仅供查询使用
	@Transient
	protected Date modifyDate_end;// 仅供查询使用

	public void updateDate() {
		this.createDate = new Date();
		this.modifyDate = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getCreateDate_start() {
		return createDate_start;
	}

	public void setCreateDate_start(Date createDate_start) {
		this.createDate_start = createDate_start;
	}

	public Date getCreateDate_end() {
		return createDate_end;
	}

	public void setCreateDate_end(Date createDate_end) {
		this.createDate_end = createDate_end;
	}

	public Date getModifyDate_start() {
		return modifyDate_start;
	}

	public void setModifyDate_start(Date modifyDate_start) {
		this.modifyDate_start = modifyDate_start;
	}

	public Date getModifyDate_end() {
		return modifyDate_end;
	}

	public void setModifyDate_end(Date modifyDate_end) {
		this.modifyDate_end = modifyDate_end;
	}

	/** 类型相同，id相同，就视为对象相同. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseEntity other = (BaseEntity) obj;
		if (id == null || other.getId() == null) {
			return false;
		} else {
			return (id.equals(other.getId()));
		}
	}

	/** 类型相同，id相同，就视为对象相同. */
	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		return prime * result + (id == null ? 0 : id.hashCode());
	}

}