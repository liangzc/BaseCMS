package com.cms.common.util;

/** 校验工具. */
public abstract class ValidateUtil {
	public static final String REGEX_IPV4 = "^((25[0-5]|2[0-4]\\d|[0-1]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[0-1]?\\d\\d?)$";
	public static final String REGEX_IPV6 = "^\\s*((([0-9A-Fa-f]{1,4}:){7}(([0-9A-Fa-f]{1,4})|:))|(([0-9A-Fa-f]{1,4}:){6}(:|((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})|(:[0-9A-Fa-f]{1,4})))|(([0-9A-Fa-f]{1,4}:){5}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4}){0,1}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){0,2}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]{1,4}){0,3}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(([0-9A-Fa-f]{1,4}:)(:[0-9A-Fa-f]{1,4}){0,4}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(:(:[0-9A-Fa-f]{1,4}){0,5}((:((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})?)|((:[0-9A-Fa-f]{1,4}){1,2})))|(((25[0-5]|2[0-4]\\d|[01]?\\d{1,2})(\\.(25[0-5]|2[0-4]\\d|[01]?\\d{1,2})){3})))(%.+)?\\s*$";
	public static final String REGEX_CHINESE = "[\u4e00-\u9fa5]*";
	public static final String REGEX_EMAIL = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
	public static final String REGEX_URL = "http://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";

	
	/** 非null，非空串. */
	public static boolean isNotEmpty(String validate) {
		if (validate == null) {
			return false;
		}
		if (validate.length() == 0) {
			return false;
		}
		return true;
	}
	
	/** 非null，非空串，不能全部都是空白字符(至少有1个非空白字符). */
	public static boolean isNotBlank(String validate) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		int strLen = validate.length();
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(validate.charAt(i))) {
				return true;
			}
		}
		return false;
	}
	
	/** 非null，非空串，不能带有任何空白字符. */
	public static boolean isNotAnyBlank(String validate) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		int strLen = validate.length();
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(validate.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	/** 非null，非空串，匹配正则表达式. */
	public static boolean isMatches(String validate, String regex) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		if (! validate.matches(regex)) {
			return false;
		}
		return true;
	}
	
	/** 非null，非空串，限制最大长度. */
	public static boolean isMaxLength(String validate, int maxLength) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		if (validate.length() > maxLength) {
			return false;
		}
		return true;
	}
	
	/** 非null，非空串，限制最小长度. */
	public static boolean isMinLength(String validate, int minLength) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		if (validate.length() < minLength) {
			return false;
		}
		return true;
	}
	
	/** 非null，非空串，限制最小最大长度. */
	public static boolean isBetweenLength(String validate, int minLength, int maxLength) {
		if (! isNotEmpty(validate)) {
			return false;
		}
		if (validate.length() > maxLength) {
			return false;
		}
		if (validate.length() < minLength) {
			return false;
		}
		return true;
	}
	
	/** 非null，非0. */
	public static boolean isNotNullNotZero(Integer validate) {
		if (validate == null) {
			return false;
		}
		if (validate == 0) {
			return false;
		}
		return true;
	}
	
	/** 非null，限制最大. */
	public static boolean isMax(Integer validate, int maxNumber) {
		if (validate == null) {
			return false;
		}
		if (validate > maxNumber) {
			return false;
		}
		return true;
	}
	
	/** 非null，限制最小. */
	public static boolean isMin(Integer validate, int minNumber) {
		if (validate == null) {
			return false;
		}
		if (validate < minNumber) {
			return false;
		}
		return true;
	}
	
	/** 数组不为null，且一定有元素，所有元素不为null. */
	public static boolean isNotEmpty(Object[] array) {
		if (array == null || array.length == 0) {
			return false;
		}
		for (Object object : array) {
			if (object == null) {
				return false;
			}
		}
		return true;
	}
	
	/** 数组元素没有任何一个为空值 */
	public static boolean notHasNull(Object... validates) {
		for (Object validate : validates) {
			if (validate == null) {
				return false;
			}
		}
		return true;
	}
}