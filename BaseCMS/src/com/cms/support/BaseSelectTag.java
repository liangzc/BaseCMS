package com.cms.support;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cms.entity.DataDict;

/**
 * @author: zhangp Date: 14-7-16 Time: 上午9:55.
 */
public class BaseSelectTag extends BaseDiyTag {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseDiyTag.class);
	private String selectValue;

	private StringBuilder sb ;
	public int doStartTag() throws JspException {
		sb = new StringBuilder();
		sb.append("<select name='").append(name).append("'");
		generateAttribute(sb);// 加入属性
		sb.append(">");
		try {
			this.pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseSelectTag）的 方法（doStartTag）异常" + e);
		}
		return EVAL_BODY_INCLUDE;
	}
	
	@Override
	public int doEndTag() throws JspException {
		List<DataDict> dataDicts = dataDictService.listDataByName(collection);
		sb = new StringBuilder();
		for (DataDict dataDict : dataDicts) {
			sb.append("<option value='").append(dataDict.getValue())
					.append("'");
			if (null != selectValue && selectValue.equals(dataDict.getValue())) {
				sb.append(" selected='selected'");
			}
			sb.append(">").append(dataDict.getDescription())
					.append("</option>");
		}
		sb.append("</select>");
		try {
			this.pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseSelectTag）的 方法（doEndTag）异常" + e);
		}
		return super.doEndTag();
	}

	public String getSelectValue() {
		return selectValue;
	}

	public void setSelectValue(String selectValue) {
		this.selectValue = selectValue;
	}
	public StringBuilder getSb() {
		return sb;
	}
	public void setSb(StringBuilder sb) {
		this.sb = sb;
	}

}
