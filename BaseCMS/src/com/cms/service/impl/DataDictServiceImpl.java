package com.cms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cms.common.util.StringUtil;
import com.cms.entity.DataDict;
import com.cms.service.DataDictService;

@Service
public class DataDictServiceImpl extends BaseServiceImpl implements DataDictService {

	@Cacheable(value="myCache")
	@Transactional(readOnly = true)
	public List<DataDict> listDataByName(String name) {
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(DataDict.class));

		// 查询条件
		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.eq("name", name));
		}

		// 查询字段
		any.add(Projections.property("id").as("id"));
		any.add(Projections.property("name").as("name"));
		any.add(Projections.property("value").as("value"));
		any.add(Projections.property("description").as("description"));

		return baseDao.findByAny(DataDict.class, any.toArray());
	}
	
	@Cacheable(value="myCache")
	@Transactional(readOnly = true)
	@Override
	public Map<String, String> mapAll() {
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(DataDict.class));

		// 查询字段
		any.add(Projections.property("name").as("name"));
		any.add(Projections.property("value").as("value"));
        List<DataDict> list = baseDao.findByAny(DataDict.class, any.toArray());
        Map<String, String> map = new HashMap<String, String>();
        for (DataDict dataDict :list) {
			map.put(dataDict.getName()+dataDict.getValue(), dataDict.getValue());
		}
        return map;
	}

	@Transactional(readOnly = true)
	public DataDict loadDataById(Integer id) {
		return baseDao.get(DataDict.class, id);
	}

	@Transactional(readOnly = true)
	public List<DataDict> listDataDicts(String name, String value, String desc,
			String nameCN, Integer pageNo, Integer pageSize) {
		// 查询参数
		pageNo = pageNo == null ? 1 : pageNo;
		pageSize = pageSize == null ? 20 : pageSize;
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(DataDict.class));

		// 查询条件
		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.like("name", "%" + name.trim() + "%"));
		}
		if (StringUtils.isNotBlank(value)) {
			any.add(Restrictions.like("value", "%" + value.trim() + "%"));
		}
		if (StringUtils.isNotBlank(desc)) {
			any.add(Restrictions.like("description", "%" + desc.trim() + "%"));
		}
		if (StringUtils.isNotBlank(nameCN)) {
			any.add(Restrictions.like("nameCN", "%" + nameCN.trim() + "%"));
		}

		// 查询字段
		any.add(Projections.property("id").as("id"));
		any.add(Projections.property("name").as("name"));
		any.add(Projections.property("value").as("value"));
		any.add(Projections.property("description").as("description"));
		any.add(Projections.property("nameCN").as("nameCN"));
		return baseDao.findByAny(DataDict.class, (pageNo - 1) * pageSize,
				pageSize, any.toArray());
	}

	@Transactional(readOnly = true)
	public Long getCount(String name, String value, String desc, String nameCN) {
		// 查询结果
		List<Criterion> any = new ArrayList<Criterion>();
		// 查询条件
		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.like("name",
					StringUtil.join("%", name.trim(), "%")));
		}
		if (StringUtils.isNotBlank(value)) {
			any.add(Restrictions.like("value",
					StringUtil.join("%", value.trim(), "%")));
		}
		if (StringUtils.isNotBlank(desc)) {
			any.add(Restrictions.like("description",
					StringUtil.join("%", desc.trim(), "%")));
		}
		if (StringUtils.isNotBlank(nameCN)) {
			any.add(Restrictions.like("nameCN",
					StringUtil.join("%", nameCN.trim(), "%")));
		}
		return baseDao.countByAny(DataDict.class,
				any.toArray(new Criterion[any.size()]));
	}

	@Transactional
	public DataDict add(DataDict dataDict) {
		baseDao.save(dataDict);
		return dataDict;
	}

	@Transactional
	public boolean modify(DataDict dataDict) {
		try {
			baseDao.update(dataDict);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	public boolean delete(Integer id) {
		try {
			baseDao.delete(baseDao.load(DataDict.class, id));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Transactional
	public Long getCount() {

		return baseDao.count(DataDict.class);
	}

	@Transactional
	public boolean queryByNameAndValue(String value, String name) {
		// 查询结果
		List<Criterion> any = new ArrayList<Criterion>();
		// 查询条件
		if (StringUtils.isNotBlank(name)) {
			any.add(Restrictions.eq("name", name.trim()));
		}
		if (StringUtils.isNotBlank(value)) {
			any.add(Restrictions.eq("value", value.trim()));
		}
		long count = baseDao.countByAny(DataDict.class,
				any.toArray(new Criterion[any.size()]));
		return count > 0 ? true : false;
	}


}
