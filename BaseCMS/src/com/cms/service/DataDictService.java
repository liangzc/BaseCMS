package com.cms.service;

import java.util.List;
import java.util.Map;

import com.cms.entity.DataDict;

public interface DataDictService {
	public List<DataDict> listDataByName(String name);
	public Map<String, String> mapAll();

	public DataDict loadDataById(Integer id);

	public List<DataDict> listDataDicts(String name,String value, String desc,String nameCN,
			Integer pageNo, Integer pageSize);
	
	public Long getCount(String name,String value, String desc,String nameCN) ;
	public Long getCount();

	public DataDict add(DataDict dataDict);

	public boolean modify(DataDict dataDict);
	/**
	 * 根据 name 和 value 查询是否存在记录
	 * @param value 
	 * @param name
	 * @return 存在返回true 否则 false
	 */
	public boolean queryByNameAndValue(String value,String name);
	
	public boolean delete(Integer id);
}