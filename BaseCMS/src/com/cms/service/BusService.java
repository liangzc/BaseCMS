package com.cms.service;

import java.util.List;

import com.cms.entity.Bus;
import com.cms.support.Result;

public interface BusService extends BaseService {

	public Result updateStatus(Integer id, Integer status);

	public List<Bus> list(Bus order, Integer pageSize, Integer pageNum);
}
