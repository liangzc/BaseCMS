package com.cms.service;

import java.util.List;

public interface BaseService {

	/** 列表查询. */
	public <T> List<T> listByExample(T exampleEntity, Integer pageNo,
			Integer pageSize);

	/** 根据id查询. */
	public Object get(Class<?> clazz, Integer id);

	/** 增加或更新实体. */
	public void saveOrUpdate(Object entity);

	/** 删除指定的实体. */
	public void delete(Object entity);

	/** 计数的实体. */
	public Long count(Object entity);

	/** 删除指定的实体. */
	public void deleteById(Class<?> clazz, Integer id);

	/** 根据条件更新. */
	public void updateByX(String hql, Object...objects);
}
