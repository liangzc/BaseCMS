package com.cms.controllers;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.entity.Action;
import com.cms.entity.weixin.Account;
import com.cms.service.ActionService;
import com.cms.service.MyBaseService;

@Controller
public class WelcomeController {
	@Autowired
	private ActionService actionService;
	@Autowired
	private MyBaseService myBaseService;

	@RequestMapping("/login")
	public String login(HttpServletRequest request,HttpServletResponse response) {
		HttpSession session = request.getSession(); 
		session.invalidate();
		response.addHeader("x-frame-options","DENY");
		return "login";
	}
	@RequestMapping("/deny")
	public String dely() {
		return "deny";
	}
	@RequestMapping("/timeOut")
	public String timeOut() {
		return "timeout";
	}

	@RequestMapping("/")
	public String index(Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		Set<Action> menus = actionService.getActionByUsername(session.getAttribute("username").toString()).getChildren();
		model.addAttribute("menus", menus);
		return "main/index";
	}
	@RequestMapping("/common/right")
	public String right(Model model,HttpServletRequest request) {
		List<Account> accounts = myBaseService.listByExample(new Account(), 0, 100);
		model.addAttribute("accounts", accounts);
		return "main/right";
	}

	public ActionService getActionService() {
		return actionService;
	}
	public void setActionService(ActionService actionService) {
		this.actionService = actionService;
	}
}