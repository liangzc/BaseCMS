package com.cms.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cms.support.MD5Util;

public class SecurityFilter implements Filter {
	private final static String  key = "88888888";

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		try {
			String userName = httpRequest.getParameter("userName");
			String sign = httpRequest.getParameter("sign");
			if ("/error".equals(httpRequest.getServletPath())) {
				chain.doFilter(request, response);
			}
			if (userName != null && sign != null
					&& sign.equals(MD5Util.getMD5(userName+key))) {
				chain.doFilter(request, response);
			} else {
				httpResponse.sendRedirect("error");
			}
		} catch (Exception e) {
			//httpResponse.sendRedirect("error");
		}

	}

	public void destroy() {
	}

}