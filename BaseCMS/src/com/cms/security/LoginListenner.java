package com.cms.security;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class LoginListenner implements HttpSessionAttributeListener {

	private Map<String, HttpSession> map = new HashMap<String, HttpSession>();

	public void attributeAdded(HttpSessionBindingEvent event) {
		String name = event.getName();

		if (name.equals("userSession")) {

			UserSession userSession = (UserSession) event.getValue();

			if (map.get(userSession.getUsername()) != null) {

				HttpSession session = map.get(userSession.getUsername());

				session.removeAttribute("userSession");

				session.invalidate();
			}
			map.put(userSession.getUsername(), event.getSession());
		}

	}

	public void attributeRemoved(HttpSessionBindingEvent event) {
		String name = event.getName();

		if (name.equals("userSession")) {

			UserSession userSession = (UserSession) event.getValue();

			map.remove(userSession.getUsername());

		}
	}

	public void attributeReplaced(HttpSessionBindingEvent event) {

	}

	public Map<String, HttpSession> getMap() {
		return map;
	}

	public void setMap(Map<String, HttpSession> map) {
		this.map = map;
	}

}
